# clipping-from-visit

Repository for implementing Clipping from Visit

Points of focus for now

1. For Hexahedral mesh, get cases for preforming clipping.

2. Based on these cases, and using new clipping tables, make new polygons
   that are a polygon zoo.
   ( Involves calculating of new points, along edges and otherwise )
   
3. Integrate both steps to make a combined CellSet


# Converting visit clip tables to VTK-m clip tables

Execute the following 

```
python parse_visit_tabs.py -s ../visit_clip_tables
```

This script will generate a file `VTKmClipTables` which contains all the offset arrays and clip tables
that can be ported to VTK-m
