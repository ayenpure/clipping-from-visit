#include <cfloat>
#include <cstdlib>
#include <fstream>
#include <sstream>
#include <vector>

#include <vtkm/cont/Timer.h>
#include <vtkm/filter/contour/ClipWithField.h>
#include <vtkm/io/VTKDataSetReader.h>
#include <vtkm/io/VTKDataSetWriter.h>

int performTrivialIsoVolume(vtkm::cont::DataSet &input,
                            char *variable,
                            vtkm::cont::DataSet &output,
                            vtkm::Float32 isoValue)
{
  vtkm::filter::contour::ClipWithField clip;
  clip.SetActiveField(std::string(variable));
  clip.SetClipValue(isoValue);
  clip.SetInvertClip(false);
  output = clip.Execute(input);

  vtkm::io::VTKDataSetWriter writer("output_inv.vtk");
  writer.WriteDataSet(output);
  return 0;
}

int parseParameters(char **argv,
                    char **filename,
                    char **variable,
                    float *isoValue)
{
  *filename = argv[1];
  *variable = argv[2];
  *isoValue = atof(argv[3]);
  return 0;
}

int main(int argc, char **argv) {
  if (argc < 3)
  {
    std::cerr << "Invalid num of arguments " << std::endl;
    exit(0);
  }
  char *filename, *variable;
  float isoValue = 0.;
  parseParameters(argv, &filename, &variable, &isoValue);

  // Read dataset
  vtkm::io::VTKDataSetReader reader(filename);
  vtkm::cont::DataSet input = reader.ReadDataSet();

  // Query original dataset
  std::cout << "Original number of Cells : "
            << input.GetCellSet().GetNumberOfCells() << std::endl;

  // Retrieve resultant dataset
  vtkm::cont::DataSet clipped;
  std::cout << "Executing trivial IsoVolume." << std::endl;
  performTrivialIsoVolume(input, variable, clipped, isoValue);

  // Query resultant dataset
  std::cout << "Filtered number of Cells : "
            << clipped.GetCellSet().GetNumberOfCells() << std::endl;

  return 0;
}
