1. Determine the convention to access new points.
   Visit and VTK-m follow different rules.

2. Parse the Visit tables and build new ones for VTK-m
   Replace the Visit shapes with VTK-m shapes
   Keep stuff that only deals with COLOR1 and NOCOLOR

3. Study how VTK interpolates new points.
   // We'll need to do 2 pases.

   A connectivity object
   Collect all points
   Collect all edges
     -- Eliminate duplicate calculations. 
   Collect new points 
     -- Use existing points and edges to perform points,
        this are ought to be unique.

4. How would you interpolate other field for the new points?
