import os
import re
import argparse

parser = argparse.ArgumentParser(prog='parse_visit_tabs.py',
                                 description='Generates VTK-m clip tables from VisIt clip Tables')
parser.add_argument('-s', '--source', help='path to directory containing VisIt Clip data')
args = parser.parse_args()

srcpath = args.source

filepre = "ClipCases"
fileext = ".C"

# Organize map as
# {
#    shapename : {
#       case : {
#         [shape cases]
#       }
#    }
#

# shapes = ["Vtx", "Lin", "Tri", "Qua", "Tet", "Hex", "Wdg", "Pyr"]
shapes = ["Vtx", "Lin", "Tri", "Qua", "Tet", "Hex", "Wdg", "Pyr"]
# shapes = ["Tri", "Qua", "Hex"]

shapeMapping = {"ST_PNT": 0,  # Map the calculation of a point to an empty case
                "ST_VTX": 1, \
                "ST_LIN": 3, \
                "ST_TRI": 5, \
                "ST_QUA": 9, \
                "ST_TET": 10, \
                "ST_HEX": 12, \
                "ST_PYR": 14, \
                "ST_WDG": 13}


def TransformToVTKm(tokens):
    vtkmTokens = []
    length = len(tokens) - 1
    for token in tokens:
        # Vertex from the original shape
        if token.strip().startswith('P'):
            vertex = int(token.strip()[1:])
            vtkmTokens.append(100 + vertex)
        # Point interpolated on the edge
        elif token.strip().startswith('E'):
            edge = ord(token.strip()[1:])
            vtkmTokens.append(edge - 65)
        # Point that is newly calculated
        elif token.strip().startswith('N'):
            vtkmTokens.append(255)
        # Shape suggesting the shape
        else:
            vtkmTokens.append(shapeMapping.get(token.strip()))
            vtkmTokens.append(length)
    return vtkmTokens


ShapeToCaseMap = {}

for shape in shapes:
    toParse = "%s%s%s" % (filepre, shape, fileext)
    print(toParse)
    # open file here
    file = open(os.path.join(srcpath, toParse), "r")
    # Now Parse the shapes to be calculated
    # For ST_PNT : Parse only if it has NOCOLOR or COLOR0
    # For all other shapes, parse only if it has COLOR0
    # Mapping cues :
    # P -> Point
    # E -> Edge
    # N -> New Point
    process = False
    CaseToShapesMap = {}
    currentCase = -1
    for line in file:
        if ("unsigned char clipShapes%s") % shape in line:
            process = True
        elif "Dummy" in line:
            process = False
        elif process:
            # Process and preserve state across lines
            if "Case" in line:
                currentCase = int(re.search(".*#(.*):.*", line).group(1))
                CaseToShapesMap[currentCase] = []
            elif "COLOR1" not in line:
                tokens = line.strip().split(",")
                # If it's a new point to calculate, remove the second entry which is
                # a limp 0, also remove the token that occurs after COLOR, it suggests
                # how many points the new point is interpolated from.
                # COLOR has no significance anymore
                print(tokens)
                if "ST_PNT" in line:
                    del tokens[1]
                    del tokens[1]
                del tokens[1]
                tokens = tokens[:-1]
                vtkmTokens = TransformToVTKm(tokens)
                outputForCase = CaseToShapesMap[currentCase]
                outputForCase.append(vtkmTokens)
                CaseToShapesMap[currentCase] = outputForCase
    ShapeToCaseMap[shape] = CaseToShapesMap
file.close()

vtkmoffset = 0
ShapeToOffsetMap = {}
# for shape, cases in ShapeToCaseMap.items() :
for shape in shapes:
    cases = ShapeToCaseMap.get(shape)
    CaseToOffsetMap = {}
    for case, outputForCase in cases.items():
        CaseToOffsetMap[case] = vtkmoffset
        # Add 1 for storing the number of shapes
        vtkmoffset += 1
        # For each shape, add storage required to
        # represent the shape
        for entry in outputForCase:
            vtkmoffset += len(entry)
    ShapeToOffsetMap[shape] = CaseToOffsetMap

outputfile = open("VTKmClipTables", "w+")

# 1. Track the offset for each type of input Cell.
shapeOffset = 0
# for shape, caseOffsets in ShapeToOffsetMap.items():
for shape in shapes:
    caseOffsets = ShapeToOffsetMap.get(shape)
    toWrite = "%d //%s\n" % (shapeOffset, shape)
    outputfile.write(toWrite)
    shapeOffset += len(caseOffsets.keys())
outputfile.write("\n\n")

# 2. Track the offset for eacb case in the type of input Cell.
chunk = 8
# for shape, caseOffsets in ShapeToOffsetMap.items():
for shape in shapes:
    caseOffsets = ShapeToOffsetMap.get(shape)
    outputfile.write("//%s\n" % shape)
    numCases = len(caseOffsets.keys())
    for i in range(0, numCases - 1, chunk):
        keysForLine = list(caseOffsets.keys())[i:i+chunk]
        valuesForLine = [caseOffsets.get(key) for key in keysForLine]
        toWrite = ""
        for value in valuesForLine:
            toWrite = "%s%d, " % (toWrite, value)
        toWrite = "%s //cases %d - %d\n" \
            % (toWrite, keysForLine[0], keysForLine[len(keysForLine) - 1])
        outputfile.write(toWrite)

outputfile.write("\n\n")

# 3. Write out the output into the file, call it the final.
# for shape, cases in ShapeToCaseMap.items():
for shape in shapes:
    cases = ShapeToCaseMap.get(shape)
    outputfile.write("// %s\n" % shape)
    for case, output in cases.items():
        outputfile.write("%d, // Case : %d\n" % (len(output), case))
        for shape in output:
            for desc in shape:
                outputfile.write("%d, " % desc)
            outputfile.write("\n")

outputfile.close()
