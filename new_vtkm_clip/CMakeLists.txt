cmake_minimum_required(VERSION 3.3 FATAL_ERROR)
project(Clip CXX)

#Find the VTK-m package
find_package(VTKm REQUIRED QUIET)

add_executable(clip Clip.cxx)
target_link_libraries(clip PRIVATE vtkm_cont)
