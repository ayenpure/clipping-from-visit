#include <vtkm/Types.h>
#include <vtkm/cont/Algorithm.h>
#include <vtkm/io/reader/VTKDataSetReader.h>
#include <vtkm/io/writer/VTKDataSetWriter.h>
#include <vtkm/worklet/WorkletMapTopology.h>
#include <vtkm/worklet/DispatcherMapTopology.h>
#include "ClippingWorklets.h"

int parseParameters(char **argv,
                    char **filename,
                    char **variable,
                    double *isoValue)
{
  *filename = argv[1];
  *variable = argv[2];
  *isoValue = atof(argv[3]);
  return 0;
}

template <typename T>
void PrintArray(vtkm::cont::ArrayHandle<T>& toPrint)
{
  vtkm::Id length = toPrint.GetNumberOfValues();
  auto portal = toPrint.GetPortalConstControl();
  for(vtkm::IdComponent i = 0; i < length; i++)
  {
    std::cout << i << ":" << portal.Get(i) << " ";
  }
  std::cout << std::endl;
}

/*template <typename ValueType, typename StorageType>
vtkm::cont::ArrayHandle<ValueType> ProcessPointField(
  vtkm::cont::ArrayHandle<ValueType, StorageType>& fieldData,
  vtkm::cont::ArrayHandle<clipping::EdgeInterpolation>& edgeInterpolations,
  vtkm::cont::ArrayHandle<vtkm::Id>& inCellInterpolationKeys,
  vtkm::cont::ArrayHandle<vtkm::Id>& inCellInterpolationInfo)
{
  using ResultType = vtkm::cont::ArrayHandle<ValueType>;
  using Worker = clipping::MapFieldHelper<ResultType>;

  ResultType output;
  Worker worker = Worker(edgeInterpolations, inCellInterpolationKeys, inCellInterpolationInfo, &output);
  worker(fieldData) ;

  return output;
}*/

struct ProcessPointFieldDynamic
{
  VTKM_CONT
  ProcessPointFieldDynamic(vtkm::cont::ArrayHandle<clipping::EdgeInterpolation>& edgeInterpolations,
                           vtkm::cont::ArrayHandle<vtkm::Id>& inCellInterpolationKeys,
                           vtkm::cont::ArrayHandle<vtkm::Id>& inCellInterpolationInfo)
  : EdgeInterpolations(edgeInterpolations)
  , InCellInterpolationKeys(inCellInterpolationKeys)
  , InCellInterpolationInfo(inCellInterpolationInfo)
  {}

  template <typename FieldType, typename Storage>
  VTKM_CONT vtkm::cont::ArrayHandle<FieldType> operator()(
    vtkm::cont::ArrayHandle<FieldType, Storage>& fieldData) const
  {
    using ResultType = vtkm::cont::ArrayHandle<FieldType>;
    using Worker = clipping::MapFieldHelper<ResultType>;

    ResultType output;
    Worker worker = Worker(this->EdgeInterpolations,
                           this->InCellInterpolationKeys,
                           this->InCellInterpolationInfo, &output);
    worker(fieldData) ;
    return output;
  }

private:
  vtkm::cont::ArrayHandle<clipping::EdgeInterpolation> EdgeInterpolations;
  vtkm::cont::ArrayHandle<vtkm::Id> InCellInterpolationKeys;
  vtkm::cont::ArrayHandle<vtkm::Id> InCellInterpolationInfo;
};

int main(int argc, char** argv)
{
  if (argc < 4)
  {
    std::cerr << "Invalid num of arguments " << std::endl;
    std::cout << "Usage : clip <input file> <input variable> <input value>" << std::endl;
    exit(EXIT_FAILURE);
  }
  char *filename, *variable;
  double isoValue = 0.;
  parseParameters(argv, &filename, &variable, &isoValue);

  // Read dataset
  vtkm::io::reader::VTKDataSetReader reader(filename);
  vtkm::cont::DataSet input = reader.ReadDataSet();

  // Query original dataset
  vtkm::cont::DynamicCellSet toProcess = input.GetCellSet(0);
/*  if(!inputCells.IsType<vtkm::cont::CellSetStructured<3>>())
  {
    std::cout << "Huh!! Cell set is not strucutred?" << std::endl;
    exit(EXIT_FAILURE);
  }
  // Get the CellSet to work on
  using StructuredType = vtkm::cont::CellSetStructured<3>;
  StructuredType toProcess = inputCells.Cast<StructuredType>();
*/
  // Get the required input fields
  clipping::ClippingData clippingData;
  vtkm::cont::Field scalar = input.GetField(variable, vtkm::cont::Field::Association::POINTS);

  // Create the required output fields.
  vtkm::cont::ArrayHandle<clipping::ClipStats> clipStats;
  vtkm::cont::ArrayHandle<vtkm::Id> clipTableIndices;

  clipping::ComputeStats statsWorklet(isoValue);
  //Send this CellSet to process
  vtkm::worklet::DispatcherMapTopology<clipping::ComputeStats> statsDispatcher(statsWorklet);
  statsDispatcher.Invoke(toProcess, scalar, clippingData, clipStats, clipTableIndices);

  /*clipping::ClipStats stat = portal.Get(0);
  std::cout << "Number of Shapes : " << stat.NumberOfCells << std::endl;
  std::cout << "Number of Indices : " << stat.NumberOfIndices << std::endl;
  std::cout << "Number of Edge points : " << stat.NumberOfEdgeIndices << std::endl;
  std::cout << "Number of Cell points : " << stat.NumberOfInCellIndices << std::endl;
  std::cout << "Number of points to interpolate : " << stat.NumberOfInCellInterpPoints << std::endl;
  std::cout << "Number of edge points to interpolate : " << stat.NumberOfInCellEdgeIndices << std::endl;*/

  clipping::ClipStats zero;
  vtkm::cont::ArrayHandle<clipping::ClipStats> cellSetStats;
  clipping::ClipStats total =
    vtkm::cont::Algorithm::ScanExclusive(clipStats, cellSetStats, clipping::ClipStats::SumOp(), zero);
  clipStats.ReleaseResources();

  vtkm::cont::ArrayHandle<vtkm::UInt8> shapes;
  vtkm::cont::ArrayHandle<vtkm::IdComponent> numberOfIndices;
  vtkm::cont::ArrayHandle<vtkm::Id> connectivity;
  vtkm::cont::ArrayHandle<vtkm::Id> offsets;
  clipping::ConnectivityExplicit connectivityObject(shapes, numberOfIndices, connectivity, offsets, total);

  //Begin Process of Constructing the new CellSet.
  vtkm::cont::ArrayHandle<vtkm::Id> edgePointReverseConnectivity;
  edgePointReverseConnectivity.Allocate(total.NumberOfEdgeIndices);
  vtkm::cont::ArrayHandle<clipping::EdgeInterpolation> edgeInterpolation;
  edgeInterpolation.Allocate(total.NumberOfEdgeIndices);

  vtkm::cont::ArrayHandle<vtkm::Id> cellPointReverseConnectivity;
  cellPointReverseConnectivity.Allocate(total.NumberOfInCellIndices);
  vtkm::cont::ArrayHandle<vtkm::Id> cellPointInterpolationKeys;
  cellPointInterpolationKeys.Allocate(total.NumberOfInCellInterpPoints);
  vtkm::cont::ArrayHandle<vtkm::Id> cellPointInterpolationInfo;
  cellPointInterpolationInfo.Allocate(total.NumberOfInCellInterpPoints);
  vtkm::cont::ArrayHandle<vtkm::Id> cellPointEdgeReverseConnectivity;
  cellPointEdgeReverseConnectivity.Allocate(total.NumberOfInCellEdgeIndices);
  vtkm::cont::ArrayHandle<clipping::EdgeInterpolation> cellPointEdgeInterpolation;
  cellPointEdgeInterpolation.Allocate(total.NumberOfInCellEdgeIndices);

  vtkm::cont::ArrayHandle<vtkm::Id> cellMapOutputToInput;
  cellMapOutputToInput.Allocate(total.NumberOfCells);

  std::cout << "Launching Generate Cell Set" << std::endl;

  clipping::GenerateCellSet cellSetWorklet(isoValue);
  //Send this CellSet to process
  vtkm::worklet::DispatcherMapTopology<clipping::GenerateCellSet> cellSetDispatcher(cellSetWorklet);
  cellSetDispatcher.Invoke(toProcess,
                           scalar,
                           clipTableIndices,
                           cellSetStats,
                           clippingData,
                           connectivityObject,
                           edgePointReverseConnectivity,
                           edgeInterpolation,
                           cellPointReverseConnectivity,
                           cellPointEdgeReverseConnectivity,
                           cellPointEdgeInterpolation,
                           cellPointInterpolationKeys,
                           cellPointInterpolationInfo,
                           cellMapOutputToInput);

  std::cout << "Finished Generate Cell Set" << std::endl;

  std::cout << "Generating interpolation arrays" << std::endl;
  // Get unique EdgeInterpolation : unique edge points.
  // LowerBound for edgeInterpolation : get index into new edge points array.
  // LowerBound for cellPoitnEdgeInterpolation : get index into new edge points array.
  vtkm::cont::ArrayHandle<clipping::EdgeInterpolation> uniqueEdgeInterpolations;
  vtkm::cont::Algorithm::SortByKey(edgeInterpolation,
                                   edgePointReverseConnectivity,
                                   clipping::EdgeInterpolation::LessThanOp());
  vtkm::cont::Algorithm::Copy(edgeInterpolation, uniqueEdgeInterpolations);
  vtkm::cont::Algorithm::Unique(uniqueEdgeInterpolations, clipping::EdgeInterpolation::EqualsOp());

  vtkm::cont::ArrayHandle<vtkm::Id> edgeInterpolationIndexToUnique;
  vtkm::cont::Algorithm::LowerBounds(uniqueEdgeInterpolations,
                                     edgeInterpolation,
                                     edgeInterpolationIndexToUnique,
                                     clipping::EdgeInterpolation::LessThanOp());

  vtkm::cont::ArrayHandle<vtkm::Id> cellInterpolationIndexToUnique;
  vtkm::cont::Algorithm::LowerBounds(uniqueEdgeInterpolations,
                                     cellPointEdgeInterpolation,
                                     cellInterpolationIndexToUnique,
                                     clipping::EdgeInterpolation::LessThanOp());

  vtkm::Id edgePointOffset = scalar.GetData().GetNumberOfValues();
  vtkm::Id inCellPointOffset = edgePointOffset + uniqueEdgeInterpolations.GetNumberOfValues();

  // Scatter these values into the connectivity array,
  // scatter indices are given in reverse connectivity.
  clipping::ScatterEdgeConnectivity scatterEdgePointConnectivity(edgePointOffset);
  vtkm::worklet::DispatcherMapField<clipping::ScatterEdgeConnectivity>
    scatterEdgeDispatcher(scatterEdgePointConnectivity);
  scatterEdgeDispatcher.Invoke(edgeInterpolationIndexToUnique, edgePointReverseConnectivity, connectivity);
  scatterEdgeDispatcher.Invoke(cellInterpolationIndexToUnique,
                               cellPointEdgeReverseConnectivity,
                               cellPointInterpolationInfo);
  //Add offset in connectivity of all new in-cell points.
  clipping::ScatterInCellConnectivity scatterInCellPointConnectivity(inCellPointOffset);
  vtkm::worklet::DispatcherMapField<clipping::ScatterInCellConnectivity>
    scatterInCellDispatcher(scatterInCellPointConnectivity);
  scatterInCellDispatcher.Invoke(cellPointReverseConnectivity, connectivity);

  std::cout << "Finished generating interpolation arrays" << std::endl;

  vtkm::cont::DataSet output;
  vtkm::cont::CellSetExplicit<> outputCellSet;

  ProcessPointFieldDynamic pointFieldProcessor(uniqueEdgeInterpolations,
                                               cellPointInterpolationKeys,
                                               cellPointInterpolationInfo);

  std::cout << "Calculating new coordinates" << std::endl;
  const vtkm::cont::CoordinateSystem& inputCoords = input.GetCoordinateSystem(0);
  auto inputCoordsArray = inputCoords.GetData();
  auto outputCoordsArray = pointFieldProcessor(inputCoordsArray);
  std::cout << "Finished Calculating new coordinates" << std::endl;

  std::cout << "Calculating new Field" << std::endl;
  //using FieldDataType = vtkm::cont::ArrayHandle<vtkm::FloatDefault>;
  //FieldDataType inputScalarArray;
  //scalar.GetData().CopyTo(inputScalarArray);
  //FieldDataType outputScalarArray = pointFieldProcessor(inputScalarArray);
  std::cout << "Finished Calculating new Field" << std::endl;

  outputCellSet.Fill(inputCoordsArray.GetNumberOfValues(), shapes, numberOfIndices, connectivity);
  vtkm::cont::CoordinateSystem outputCoords(inputCoords.GetName(), outputCoordsArray);
  //vtkm::cont::Field outputField(scalar.GetName(), scalar.GetAssociation(), outputScalarArray);

  output.AddCellSet(outputCellSet);
  output.AddCoordinateSystem(outputCoords);
  //output.AddField(outputField);

  vtkm::io::writer::VTKDataSetWriter writer("output.vtk");
  writer.WriteDataSet(output);

  return 0;
}
