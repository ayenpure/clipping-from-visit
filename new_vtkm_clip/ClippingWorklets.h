#include <vtkm/Types.h>
#include <vtkm/worklet/WorkletMapTopology.h>
#include <vtkm/worklet/DispatcherMapTopology.h>

#include <vtkm/worklet/Keys.h>
#include <vtkm/worklet/WorkletReduceByKey.h>
#include <vtkm/worklet/DispatcherReduceByKey.h>

#include "ClippingTables.h"
#include "ClippingObjects.h"

namespace clipping
{

/* Restrict types for tempate */
struct TypeClipStats : vtkm::ListTagBase<clipping::ClipStats>
{};

struct TypeEdgeInterp : vtkm::ListTagBase<clipping::EdgeInterpolation>
{};


class ComputeStats : public vtkm::worklet::WorkletMapPointToCell
{
public:
  VTKM_CONT
  ComputeStats(vtkm::FloatDefault _value)
  : Value(_value)
  {}

  using ControlSignature = void(CellSetIn,
                                FieldInPoint<Scalar>,
                                ExecObject clippingData,
                                FieldOutCell<TypeClipStats>,
                                FieldOutCell<IdType>);

  using ExecutionSignature = void(CellShape, WorkIndex, PointCount, _2, _3, _4, _5);

  using InputDomain = _1;

  template<typename CellShapeTag,
           typename WorkIndexType,
           typename ScalarFieldVec,
           typename ClippingData,
           typename ClipStatsType>
  VTKM_EXEC
  void operator()(const CellShapeTag shape,
                  const WorkIndexType workIndex,
                  const vtkm::Id pointCount,
                  const ScalarFieldVec& scalars,
                  const ClippingData& clippingData,
                  ClipStatsType& clipStat,
                  vtkm::Id& clipDataIndex) const
  {
    (void)shape; // C4100 false positive workaround
    //const vtkm::Id mask[] = { 1, 2, 4, 8, 16, 32, 64, 128 };
    vtkm::Id caseId = 0;
    clipping::ClipStats _clipStat;
    for(vtkm::IdComponent iter = pointCount - 1; iter  >= 0; iter--)
    {
      if(static_cast<vtkm::Float64>(scalars[iter]) <= this->Value)
        caseId++;
      if(iter > 0)
        caseId *= 2;
    }
    //caseId = 87;
    std::cout << "Mapped cell " << workIndex << " to case : " << (vtkm::Id)caseId << std::endl;
    vtkm::Id index = clippingData.GetCaseIndex(shape.Id, caseId);
    clipDataIndex = index;
    std::cout << "Got the index as : " << index << std::endl;
    vtkm::Id numberOfCells = clippingData.ValueAt(index++);
    std::cout << "Got number of Cells as : " << numberOfCells << std::endl;
    _clipStat.NumberOfCells = numberOfCells;
    for(vtkm::IdComponent shapes = 0; shapes < numberOfCells; shapes++)
    {
      vtkm::Id cellShape = clippingData.ValueAt(index++);
      vtkm::Id numberOfIndices = clippingData.ValueAt(index++);
      if(cellShape == 0)
      {
        --_clipStat.NumberOfCells;
        // Shape is 0, which is a case of interpolating new point within a cell
        // Gather stats for later operation.
        _clipStat.NumberOfInCellPoints = 1;
        _clipStat.NumberOfInCellInterpPoints = numberOfIndices;
        for(vtkm::IdComponent points = 0; points < numberOfIndices; points++, index++)
        {
          //Find how many points need to be calculated using edge interpolation.
          vtkm::Id element = clippingData.ValueAt(index);
          _clipStat.NumberOfInCellEdgeIndices += (element < 100) ? 1 : 0;
        }
      }
      else
      {
        // Collect number of indices required for storing current shape
        _clipStat.NumberOfIndices += numberOfIndices;
        // Collect number of new points
        for(vtkm::IdComponent points = 0; points < numberOfIndices; points++, index++)
        {
          //Find how many points need to found using edge interpolation.
          vtkm::Id element = clippingData.ValueAt(index);
          if(element == -1)
          {
            _clipStat.NumberOfInCellIndices++;
          }
          else if(element < 100)
          {
            _clipStat.NumberOfEdgeIndices++;
          }
        }
      }
      clipStat = _clipStat;
    }
  }

private:
  vtkm::Float64 Value;
};

class GenerateCellSet : public vtkm::worklet::WorkletMapPointToCell
{
public:
  VTKM_CONT
  GenerateCellSet(vtkm::Float64 value)
  : Value(value)
  {}

  using ControlSignature = void(CellSetIn,
                                FieldInPoint<Scalar>,
                                FieldInCell<IdType> clipTableIndices,
                                FieldInCell<TypeClipStats> clipStats,
                                ExecObject clipTables,
                                ExecObject connectivityObject,
                                WholeArrayOut<IdType> edgePointReverseConnectivity,
                                WholeArrayOut<TypeEdgeInterp> edgePointInterpolation,
                                WholeArrayOut<IdType> inCellReverseConnectivity,
                                WholeArrayOut<IdType> inCellEdgeReverseConnectivity,
                                WholeArrayOut<TypeEdgeInterp> inCellEdgeInterpolation,
                                WholeArrayOut<IdType> inCellInterpolationKeys,
                                WholeArrayOut<IdType> inCellInterpolationInfo,
                                WholeArrayOut<IdType> cellMapOutputToInput);

  using ExecutionSignature =
    void(CellShape, WorkIndex, FromIndices, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11, _12, _13, _14);

  template <typename CellShapeTag,
            typename WorkIndexType,
            typename PointVecType,
            typename ScalarVecType,
            typename ClipIndexType,
            typename ClipStatsType,
            typename ClippingData,
            typename ConnectivityObject,
            typename IdArrayType,
            typename EdgeInterpolationPortalType>
  VTKM_EXEC
  void operator()(const CellShapeTag shape,
                  const WorkIndexType workIndex,
                  const PointVecType points,
                  const ScalarVecType scalars,
                  const ClipIndexType clipDataIndex,
                  const ClipStatsType clipStats,
                  const ClippingData& clippingData,
                  ConnectivityObject& connectivityObject,
                  IdArrayType& edgePointReverseConnectivity,
                  EdgeInterpolationPortalType& edgePointInterpolation,
                  IdArrayType& inCellReverseConnectivity,
                  IdArrayType& inCellEdgeReverseConnectivity,
                  EdgeInterpolationPortalType& inCellEdgeInterpolation,
                  IdArrayType& inCellInterpolationKeys,
                  IdArrayType& inCellInterpolationInfo,
                  IdArrayType& cellMapOutputToInput) const
  {
    std::cout << "Processing Cell : " << workIndex << std::endl;
    (void)shape;
    vtkm::Id clipIndex = clipDataIndex;

    // Start index for the cells of this case.
    vtkm::Id cellIndex = clipStats.NumberOfCells;
    // Start index to store connevtivity of this case.
    vtkm::Id connectivityIndex = clipStats.NumberOfIndices;
    // Start indices for reverse mapping into connectivity for this case.
    vtkm::Id edgeIndex = clipStats.NumberOfEdgeIndices;
    vtkm::Id inCellIndex = clipStats.NumberOfInCellIndices;
    vtkm::Id inCellPoints = clipStats.NumberOfInCellPoints;
    // Start Indices to keep track of interpolation points for new cell.
    vtkm::Id inCellInterpPointIndex = clipStats.NumberOfInCellInterpPoints;
    vtkm::Id inCellEdgeInterpIndex = clipStats.NumberOfInCellEdgeIndices;

    // Iterate over the shapes for the current cell and begin to fill connectivity.
    vtkm::Id numberOfCells = clippingData.ValueAt(clipIndex++);
    for(vtkm::Id cell = 0; cell < numberOfCells; ++cell)
    {
      vtkm::Id cellShape = clippingData.ValueAt(clipIndex++);
      vtkm::Id numberOfPoints = clippingData.ValueAt(clipIndex++);
      if(cellShape == 0)
      {
        // Case for a new cell point.

        // 1. Output the input cell id for which we need to generate new point.
        // 2. Output number of points used for interpolation.
        // 3. If vertex
        //    - Add vertex to connectivity interpolation information.
        // 4. If edge
        //    - Add edge interpolation information for new points.
        //    - Reverse connectivity map for new points.
        // Make an array which has all the elements that need to be used
        // for interpolation.
        for(vtkm::IdComponent point = 0; point < numberOfPoints; point++, inCellInterpPointIndex++, clipIndex++)
        {
          vtkm::IdComponent entry = static_cast<vtkm::IdComponent>(clippingData.ValueAt(clipIndex));
          inCellInterpolationKeys.Set(inCellInterpPointIndex, workIndex);
          if(entry >= 100)
          {
            inCellInterpolationInfo.Set(inCellInterpPointIndex, points[entry - 100]);
          }
          else
          {
            clipping::EdgeVec edge = clippingData.GetEdge(shape.Id, entry);
            VTKM_ASSERT(edge[0] != 255);
            VTKM_ASSERT(edge[1] != 255);
            clipping::EdgeInterpolation ei;
            ei.Vertex1 = points[edge[0]];
            ei.Vertex2 = points[edge[1]];
            // For consistency purposes keep the points ordered.
            if(ei.Vertex1 > ei.Vertex2)
            {
              this->swap(ei.Vertex1, ei.Vertex2);
              this->swap(edge[0], edge[1]);
            }
            ei.Weight = static_cast<vtkm::Float64>(scalars[edge[0]] - this->Value) /
              static_cast<vtkm::Float64>(scalars[edge[1]] - scalars[edge[0]]);

            inCellEdgeReverseConnectivity.Set(inCellEdgeInterpIndex, inCellInterpPointIndex);
            inCellEdgeInterpolation.Set(inCellEdgeInterpIndex, ei);
            inCellEdgeInterpIndex++;
          }
        }
      }
      else
      {
        // Just a normal cell, generate edge representations,

        // 1. Add cell type to connectivity information.
        // 2. If vertex
        //    - Add vertex to connectivity information.
        // 3. If edge point
        //    - Add edge to edge points
        //    - Add edge point index to edge point reverse connectivity.
        // 4. If cell point
        //    - Add cell point index to connectivity
        //      (as there is only one cell point per required cell)
        // 5. Store input cell index against current cell for mapping cell data.
        connectivityObject.SetCellShape(cellIndex, cellShape);
        connectivityObject.SetNumberOfIndices(cellIndex, numberOfPoints);
        connectivityObject.SetIndexOffset(cellIndex, connectivityIndex);
        for(vtkm::IdComponent point = 0; point < numberOfPoints; point++, clipIndex++)
        {
          vtkm::IdComponent entry = static_cast<vtkm::IdComponent>(clippingData.ValueAt(clipIndex));
          if(entry >= 100) // existing vertex
          {
            connectivityObject.SetConnectivity(connectivityIndex++, points[entry - 100]);
          }
          else if(entry != -1)  // case of a new edge point
          {
            clipping::EdgeVec edge = clippingData.GetEdge(shape.Id, entry);
            VTKM_ASSERT(edge[0] != 255);
            VTKM_ASSERT(edge[1] != 255);
            clipping::EdgeInterpolation ei;
            ei.Vertex1 = points[edge[0]];
            ei.Vertex2 = points[edge[1]];
            // For consistency purposes keep the points ordered.
            if(ei.Vertex1 > ei.Vertex2)
            {
              this->swap(ei.Vertex1, ei.Vertex2);
              this->swap(edge[0], edge[1]);
            }
            ei.Weight = static_cast<vtkm::Float64>(scalars[edge[0]] - this->Value) /
              static_cast<vtkm::Float64>(scalars[edge[1]] - scalars[edge[0]]);
            //Add to set of new edge points
            //Add reverse connectivity;
            edgePointReverseConnectivity.Set(edgeIndex, connectivityIndex++);
            edgePointInterpolation.Set(edgeIndex, ei);
            edgeIndex++;
          }
          else // case of cell point interpolation
          {
            // Add index of the corresponding cell point.
            inCellReverseConnectivity.Set(inCellIndex++, connectivityIndex);
            connectivityObject.SetConnectivity(connectivityIndex, inCellPoints);
            connectivityIndex++;
          }
        }
        cellMapOutputToInput.Set(cellIndex, workIndex);
        ++cellIndex;
      }
    }
  }

  template < typename T>
  VTKM_EXEC void swap(T& v1, T& v2) const
  {
    T temp = v1;
    v1 = v2;
    v2 = temp;
  }

private:
  vtkm::Float64 Value;
  vtkm::Id CellPointOffset;
};

class ScatterEdgeConnectivity : public vtkm::worklet::WorkletMapField
{
public:
  VTKM_CONT
  ScatterEdgeConnectivity(vtkm::Id edgePointOffset)
  : EdgePointOffset(edgePointOffset)
  {}

  using ControlSignature = void(FieldIn<IdType> sourceValue,
                                FieldIn<IdType> destinationIndices,
                                WholeArrayOut<IdType> destinationData);

  using ExecutionSignature = void(_1, _2, _3);

  using InputDomain = _1;

  template<typename ConnectivityDataType>
  VTKM_EXEC void operator()(const vtkm::Id sourceValue,
                            const vtkm::Id destinationIndex,
                            ConnectivityDataType& destinationData) const
 {
   destinationData.Set(destinationIndex, (sourceValue + EdgePointOffset));
 }
private:
  vtkm::Id EdgePointOffset;
};

class ScatterInCellConnectivity : public vtkm::worklet::WorkletMapField
{
public:
  VTKM_CONT
  ScatterInCellConnectivity(vtkm::Id inCellPointOffset)
  : InCellPointOffset(inCellPointOffset)
  {}

  using ControlSignature = void(FieldIn<IdType> destinationIndices,
                                WholeArrayOut<IdType> destinationData);

  using ExecutionSignature = void(_1, _2);

  using InputDomain = _1;

  template<typename ConnectivityDataType>
  VTKM_EXEC void operator()(const vtkm::Id destinationIndex,
                            ConnectivityDataType& destinationData) const
 {
   auto sourceValue = destinationData.Get(destinationIndex);
   destinationData.Set(destinationIndex, (sourceValue + InCellPointOffset));
 }
private:
  vtkm::Id InCellPointOffset;
};

template <typename T>
VTKM_EXEC_CONT T Scale(const T& val, vtkm::Float64 scale)
{
  return static_cast<T>(scale*static_cast<vtkm::Float64>(val));
}

template <typename T, vtkm::IdComponent NumComponents>
VTKM_EXEC_CONT vtkm::Vec<T, NumComponents> Scale(const vtkm::Vec<T, NumComponents>& val,
                                                 vtkm::Float64 scale)
{
  return val * scale;
}

template <typename ArrayType>
class MapFieldHelper
{
public:
  using ValueType = typename ArrayType::ValueType;
  struct TypeMappedValue : vtkm::ListTagBase<ValueType>
  {};

  MapFieldHelper(vtkm::cont::ArrayHandle<EdgeInterpolation> edgeInterpolationArray,
                 vtkm::cont::ArrayHandle<vtkm::Id> inCellInterpolationKeys,
                 vtkm::cont::ArrayHandle<vtkm::Id> inCellInterpolationInfo,
                 ArrayType* output)
  : EdgeInterpolationArray(edgeInterpolationArray)
  , InCellInterpolationKeys(inCellInterpolationKeys)
  , InCellInterpolationInfo(inCellInterpolationInfo)
  , Output(output)
  {}

  class PerformEdgeInterpolations : public vtkm::worklet::WorkletMapField
  {
  public:
    PerformEdgeInterpolations(vtkm::Id edgePointsOffset)
    : EdgePointsOffset(edgePointsOffset)
    {}

    using ControlSignature = void(FieldIn<TypeEdgeInterp> edgeInterpolations,
                                  WholeArrayInOut<> outputField);

    using ExecutionSignature = void(_1, _2, WorkIndex);

    template <typename EdgeInterp, typename OutputFieldPortal>
    VTKM_EXEC void operator()(const EdgeInterp& ei,
                              OutputFieldPortal& field,
                              const vtkm::Id workIndex) const
    {
      using T = typename OutputFieldPortal::ValueType;
      T v1 = field.Get(ei.Vertex1);
      T v2 = field.Get(ei.Vertex2);
      field.Set(this->EdgePointsOffset + workIndex,
                static_cast<T>(Scale(T(v1 - v2), ei.Weight) + v1));
    }
  private:
    vtkm::Id EdgePointsOffset;
  };

  class PerformInCellInterpolations : public vtkm::worklet::WorkletReduceByKey
  {
  public:
    using ControlSignature = void(KeysIn keys,
                                  ValuesIn<TypeMappedValue> toReduce,
                                  ReducedValuesOut<TypeMappedValue> centroid);

    using ExecutionSignature = void(_1, WorkIndex, _2, _3);

    using ScatterType = vtkm::worklet::ScatterIdentity;

    template <typename MappedValueVecType,
              typename MappedValueType>
    VTKM_EXEC void operator()(const vtkm::Id key,
                              const vtkm::Id workIndex,
                              const MappedValueVecType& toReduce,
                              MappedValueType& centroid) const
    {
      vtkm::IdComponent numValues = toReduce.GetNumberOfComponents();
      MappedValueType sum = toReduce[0];
      for(vtkm::IdComponent i = 1; i < numValues; i++)
      {
        MappedValueType value = toReduce[i];
        sum = sum + value;
      }
      std::cout << sum << std::endl;
      centroid = sum / static_cast<MappedValueType>(numValues);
    }
  };

  class GatherValuesWorklet : public vtkm::worklet::WorkletMapField
  {
  public:
    using ControlSignature = void(FieldIn<> index,
                                  WholeArrayIn<TypeMappedValue> values,
                                  FieldOut<> output);

    using ExecutionSignature = void(_1, _2, _3);

    template <typename MappedValueType,
              typename MappedValueArrayType>
    VTKM_EXEC void operator()(const vtkm::Id index,
                              const MappedValueArrayType& values,
                              MappedValueType& output) const
    {
      output = values.Get(index);
    }
  };

  template <typename Storage>
  VTKM_CONT void operator()(vtkm::cont::ArrayHandle<ValueType, Storage>& field) const
  {
    vtkm::worklet::Keys<vtkm::Id> interpolationKeys(InCellInterpolationKeys);

    vtkm::Id numberOfOriginalValues = field.GetNumberOfValues();
    vtkm::Id numberOfEdgePoints = EdgeInterpolationArray.GetNumberOfValues();
    vtkm::Id numberOfInCellPoints = interpolationKeys.GetUniqueKeys().GetNumberOfValues();

    ArrayType result;
    result.Allocate(numberOfOriginalValues + numberOfEdgePoints + numberOfInCellPoints);
    vtkm::cont::Algorithm::CopySubRange(field, 0, numberOfOriginalValues, result);

    PerformEdgeInterpolations edgeInterpWorklet(numberOfOriginalValues);
    vtkm::worklet::DispatcherMapField<PerformEdgeInterpolations> edgeInterpDispatcher(edgeInterpWorklet);
    edgeInterpDispatcher.Invoke(this->EdgeInterpolationArray, result);

    //Perform a gather on output to get all required values for calculation of centroids using the interpolation info array.
    vtkm::cont::ArrayHandle<ValueType> gatheredValues;
    vtkm::worklet::DispatcherMapField<GatherValuesWorklet> gatherDispatcher;
    gatherDispatcher.Invoke(InCellInterpolationInfo, result, gatheredValues);

    vtkm::cont::ArrayHandle<ValueType> reducedValues;
    vtkm::worklet::DispatcherReduceByKey<PerformInCellInterpolations> inCellInterpolationDispatcher;
    inCellInterpolationDispatcher.Invoke(interpolationKeys, gatheredValues, reducedValues);
    vtkm::Id inCellPointsOffset = numberOfOriginalValues + numberOfEdgePoints;
    vtkm::cont::Algorithm::CopySubRange(reducedValues,
                                        0,
                                        reducedValues.GetNumberOfValues(),
                                        result,
                                        inCellPointsOffset);
    *(this->Output) = result;
  }

private:
  vtkm::cont::ArrayHandle<EdgeInterpolation> EdgeInterpolationArray;
  vtkm::cont::ArrayHandle<vtkm::Id> InCellInterpolationKeys;
  vtkm::cont::ArrayHandle<vtkm::Id> InCellInterpolationInfo;
  vtkm::Id EdgePointsOffset;
  ArrayType* Output;
};

} // namespace clipping
